$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 1500
    });

    // Eventos en el modal
    $('#contacto').on('show.bs.modal', function (event) {
        console.log('show')
        $('#infoBtn').removeClass('btn-outline-success');
        $('#infoBtn').addClass('btn-outline-primary');
        $('#infoBtn').prop('disabled',true);
    });

    $('#contacto').on('shown.bs.modal', function (event) {
        console.log('shown')
    });

    $('#contacto').on('hide.bs.modal', function (event) {
        console.log('hide')
        $('#infoBtn').removeClass('btn-outline-primary');
        $('#infoBtn').addClass('btn-outline-success');
    });

    $('#contacto').on('hidden.bs.modal', function (event) {
        console.log('hidden')
        $('#infoBtn').prop('disabled',false);
    });

});